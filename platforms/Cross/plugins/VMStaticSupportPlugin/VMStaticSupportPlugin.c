#include "SqModule.h"

extern struct SqModule display_null;
extern struct SqModule sound_null;

void  *dlopen(const char *name, int a) {
    return 0;
}

void  *dlsym(void *__restrict handle, const char *__restrict func) {
    if (strcmp(func, "display_null") == 0) {
      return &display_null;
    }
    if (strcmp(func, "display_x11") == 0) {
      return &display_null;
    }
    if (strcmp(func, "sound_null") == 0) {
      return &sound_null;
    }
    return 0;
}