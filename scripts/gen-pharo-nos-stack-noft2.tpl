NonInteractiveTranscript stdout install.
Gofer new
	url: 'filetree://${CMAKE_SOURCE_DIR}/mc';
	package: 'VMMakerStatic-Config';
	package: 'VMMakerStatic-Libs';
	package: 'VMMakerStatic-Plugins';
	package: 'SqueakNOS-VM';
	load.
	
(Smalltalk at: #PharoNOSVMConfig) new
	platformsDir: '${CMAKE_SOURCE_DIR}/platforms';
	addInternalPlugins: #(  ); "without freetype2"
	generateSources; 
	generate. 