NonInteractiveTranscript stdout install.
Gofer new
	url: 'filetree://${CMAKE_SOURCE_DIR}/mc';
	package: 'NativeBoost-CogPlugin';
	package: 'VMMakerStatic-Config';
	package: 'VMMakerStatic-Libs';
	package: 'VMMakerStatic-Plugins';
	package: 'SqueakNOS-VM';
	load.

(Smalltalk at: #PharoNOSCogVMConfig) new
	platformsDir: '${CMAKE_SOURCE_DIR}/platforms';
	generateSources; 
	generate. 
