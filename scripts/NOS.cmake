set(PHARO_HEADLESS ./pharo-vm/pharo --nodisplay)
set(ST_PLATFORM_NAME "PharoNOS" CACHE STRING "Default VM platform")
set(ST_SCRIPT "gen-pharo-nos-stack" CACHE STRING "VM script template")