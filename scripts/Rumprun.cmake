INCLUDE(CMakeForceCompiler)

#set(CMAKE_SYSTEM_NAME RumprunUnix)
#set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_NAME Linux)

#set(CMAKE_SYSROOT /home/devel/rasp-pi-rootfs)
set(CMAKE_STAGING_PREFIX /home/johnny/stage)

SET(CMAKE_FIND_ROOT_PATH /home/johnny/dev/rumprun/rumprun)

set(CMAKE_C_COMPILER /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-gcc)
set(CMAKE_C_COMPILER_ID_RUN TRUE)
set(CMAKE_C_COMPILER_ID GNU)
#CMAKE_FORCE_C_COMPILER(/home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-gcc GNU)
set(CMAKE_CXX_COMPILER /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-g++)
set(CMAKE_CXX_COMPILER_ID_RUN TRUE)
set(CMAKE_CXX_COMPILER_ID GNU)
#CMAKE_FORCE_CXX_COMPILER(/home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-g++ GNU)
##set(CMAKE_AR /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-ar CACHE FILEPATH "Archiver")
##set(CMAKE_LINKER /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-ld CACHE FILEPATH "Linker")
##set(CMAKE_LINKER_ID GNU)
set(CMAKE_NM /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-nm)
set(CMAKE_OBJCOPY /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-objcopy)
set(CMAKE_OBJDUMP /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-objdump)
##set(CMAKE_RANLIB /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-ranlib CACHE FILEPATH "Indexer")
##set(CMAKE_STRIP /home/johnny/dev/rumprun/rumprun/bin/i486-rumprun-netbsdelf-strip)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

#set_target_properties(hello PROPERTIES LINKER_LANGUAGE CXX)
#set(UNIX 1 CACHE STRING "OS")

#include(Platform/UnixPaths)