NonInteractiveTranscript stdout install.
Author useAuthor: 'LoadVMMaker' during: [
	Gofer new
		url: 'filetree://${CMAKE_SOURCE_DIR}/mc';
		package: 'VMMakerStatic-Config';
		package: 'VMMakerStatic-Libs';
		package: 'VMMakerStatic-Plugins';
		load.
].
(Smalltalk at: #PharoSpur32UnixStaticConfig) new
	generationType: #release;
	addExternalPlugins: #( );
	addInternalPlugins: #( UnixOSProcessPlugin FT2Plugin "SqueakSSLPlugin" );
	addThirdpartyLibraries: #( 
		'libssh2' 
		'libgit2'
		'libsdl2');
	platformsDir: '${CMAKE_SOURCE_DIR}/platforms';
	generateSources; 
	generate.