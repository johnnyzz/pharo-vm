NonInteractiveTranscript stdout install.
PharoSpur32WindowsConfig new  
	generationType: #release;
	addExternalPlugins: #( FT2Plugin SqueakSSLPlugin );
	addInternalPlugins: #( Win32OSProcessPlugin );
	addThirdpartyLibraries: #(
		'cairo'
		'libssh2'
		'libgit2'
		'libsdl2');
	platformsDir: '${CMAKE_SOURCE_DIR}/platforms';
	generateSources; 
	generate.