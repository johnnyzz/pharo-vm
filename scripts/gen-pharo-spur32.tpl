NonInteractiveTranscript stdout install.
PharoSpur32UnixConfig new
	generationType: #release;
	addExternalPlugins: #( FT2Plugin SqueakSSLPlugin );
	addInternalPlugins: #( UnixOSProcessPlugin  );
	addThirdpartyLibraries: #( 
		'libssh2' 
		'libgit2'
		'libsdl2');
	platformsDir: '${CMAKE_SOURCE_DIR}/platforms';
	generateSources; 
	generate.