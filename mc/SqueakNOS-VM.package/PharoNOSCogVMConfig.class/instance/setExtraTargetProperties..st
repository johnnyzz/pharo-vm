utils
setExtraTargetProperties: maker


	maker addExternalLibraries: 
		#(
			"'m'"  "math lib"
			"'dl'"  "dynamic loader"
			"'pthread'" "posix threads" 
			"'strcmp.o'"
		).
		
	maker set: 'EXECUTABLE_OUTPUT_PATH' toString: '${outputDir}'.
	"self addVMDrivers: maker."
	
	maker setTargetProperties: 'LINK_FLAGS "-static -m32 -nostdlib -lm -Xlinker -r"'.
		
	maker puts: 'set_source_files_properties( ${srcVMDir}/cogit.c PROPERTIES 
		COMPILE_FLAGS "-O1 -fno-omit-frame-pointer -momit-leaf-frame-pointer -mno-rtd -mno-accumulate-outgoing-args")'.
		
	
	maker 
		cmd: 'set_source_files_properties'
		params: ' ${targetPlatform}/vm/sqUnixHeartbeat.c PROPERTIES 
		COMPILE_FLAGS "-O1 -fno-omit-frame-pointer -mno-rtd -mno-accumulate-outgoing-args"'.
						
