sources
platformSources
	"files added from platform/unix/vm dir "
	^ #(
	'console.c'
	'framebuffer.c'
	'ints.c'
	'nos.c'
	'paging.c'
	'serial.c'
	'sqGlue.c'
	'sqLibc.c'
	'sqMinimal.c'
	'sqPlatformSpecific.c'
	)