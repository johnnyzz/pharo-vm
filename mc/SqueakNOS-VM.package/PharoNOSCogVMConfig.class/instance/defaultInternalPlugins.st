plugins
defaultInternalPlugins
" took from unixbuild/bld/plugins.int"
	^ #(
		"FFIPlugin"
		"CogNativeBoostPlugin" "COG"
		ThreadedIA32FFIPlugin
		SqueakNOSPlugin
		ADPCMCodecPlugin 
		"AsynchFilePlugin" 
		BalloonEnginePlugin "B2DPlugin" 
		BitBltSimulation "BitBltPlugin"
		BMPReadWriterPlugin 
		"CroquetPlugin" 
		DeflatePlugin  "ZipPlugin"
		"DropPlugin" 
		"DSAPlugin" "DSAPrims" 
		FFTPlugin 
		FT2Plugin
		"FileCopyPlugin" 
		"FilePlugin"
		FloatArrayPlugin 
		"FloatMathPlugin" 
		IA32ABIPlugin "IA32ABI"
		"JoystickTabletPlugin" 
		"KlattSynthesizerPlugin" "Klatt"
		LargeIntegersPlugin "LargeIntegers"
		Matrix2x3Plugin 
		"MIDIPlugin" 
		MiscPrimitivePlugin 
		"Mpeg3Plugin"  	"fixme"
		"SecurityPlugin" 
		SerialPlugin
		"SocketPlugin" 
		"SoundCodecPlugin" "SoundCodecPrims"
		SoundGenerationPlugin 
		"SoundPlugin" 
		StarSqueakPlugin 
		SurfacePlugin	 "fixme???"
		"LocalePlugin"
		"UnixAioPlugin"
		"UnixOSProcessPlugin"		
		"SDL2DisplayPlugin"
		)