settings
extraVMSettings: maker
	| versionC |
	"self generateConfigH."
	
	"add a generated version.c"
	
	versionC := 'version.c'.
	
	maker cmd: 'add_custom_command' params: 'OUTPUT ',
		versionC , '
		COMMAND sh ${platformsDir}/PharoNOS/config/verstamp ',versionC,' gcc
		COMMENT "Generating version.c"
	'.
	