plugin extra rules
configureFT2Plugin: maker 
	"extra rules for FT2Plugin" 
	| lib |
	lib := self addThirdpartyLibrary: 'freetype2-static'.

	maker addExternalLibrary: lib libraryFileName.
	maker includeDirectories: lib includeDir.
	maker set: 'FT2Plugin_dependencies' to: 'freetype2-static'.

