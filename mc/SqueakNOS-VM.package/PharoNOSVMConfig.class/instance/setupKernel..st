utils
setupKernel: maker

	maker cmd: 'add_custom_command' params: 'OUTPUT ${KERNEL}
		COMMAND ${CMAKE_ASM_COMPILER} -m32 -nostdlib -o loader.o -c ${targetPlatform}/boot/loader.s
		COMMAND ${CMAKE_LINKER} -m elf_i386 -nostdlib -o ${buildDir}/${KERNEL} --script=${targetPlatform}/boot/kernel.ld loader.o $<TARGET_FILE:NOS.obj>
		DEPENDS NOS.obj
		COMMENT "Baking NOS kernel ... "
		WORKING_DIRECTORY "${buildDir}"
	'.