settings
extraVMSettings: maker
	| versionC |
	"self generateConfigH."

	maker cmd: 'enable_language' params: 'ASM'.

	"add a generated version.c"
	versionC := 'version.c'.
	
	maker cmd: 'add_custom_command' params: 'OUTPUT ',
		versionC , '
		COMMAND sh ${platformsDir}/unix/config/verstamp ',versionC,' gcc
		COMMENT "Generating version.c"
	'.
	
	maker includeDirectories:  '${thirdpartyDir}/out/include'.
	maker linkDirectories: '${thirdpartyDir}/out/lib'.
	
	maker set: 'KERNEL' to: 'NOS.kernel'.
	maker set: 'MCOPY' to: 'mcopy'. "mtools utility for copy"
	maker set: 'MMD' to: 'mmd'. "mtools utility for mkdir"
	maker set: 'nosBaseImageFiles' to: '${buildDir}/image/PharoNOS.image ${buildDir}/image/PharoNOS.changes'.

	self setupLibcObjects: maker.
	"self setupKernel: maker."
	self setupDiskImage: maker.
	self setupSmalltalkImage: maker.

	maker cmd: 'add_custom_target' params: 'nos-filesystem DEPENDS ${KERNEL} ${nosBaseImageFiles} NOS.fat32'.
	maker cmd: 'add_custom_target' params: 'disk-image DEPENDS NOS.img grub-installed.stamp'.
	maker cmd: 'add_custom_target' params: 'nos-image DEPENDS ${nosBaseImageFiles}'.
	maker cmd: 'add_custom_target' params: 'nos ALL DEPENDS ${KERNEL} disk-image'.
	
	maker cmd: 'add_custom_target' params: 'clean-disk-image
		COMMAND ${CMAKE_COMMAND} -E remove NOS.img NOS.fat32 grub-installed.stamp ${nosBaseImageFiles}
		WORKING_DIRECTORY "${buildDir}"
	'.
	
	maker cmd: 'install' params: 'FILES ${nosBaseImageFiles} "${buildDir}/image/pharo-vm/PharoV40.sources" "${buildDir}/NOS.img"
		DESTINATION "${outputDir}"
	'.
