accessing
libcObjectsString
	^ String streamContents: [ :out | 
		self libcObjects do: [ :each |
				out nextPutAll: each
			] separatedBy: [ out nextPutAll: ' ' ]
		]