accessing
libcObjects
	^ #( 
	"STRING"
	'strchr.o' 'strcpy.o' 'strcmp.o' 'strncpy.o' 'strlen.o' 
	'strcat.o' 'stpcpy.o' 'stpncpy.o' 'strrchr.o' 'strchrnul.o' 'strncmp.o' 
	'memchr.o' 'memrchr.o' 'memmove.o' 'memcpy.o' 'qsort.o'  
	"MATH"
	'sin.o' '__sin.o' '__cos.o' '__rem_pio2.o' '__rem_pio2_large.o' 
	'sqrt.o' 'modf.o' 'log.o' 'exp.o' 'atol.o' 'atan.o' 'frexp.o' 'frexpl.o' 'scalbn.o' 'ldexpl.o' 'floor.o'
	'longjmp.o' 'setjmp.o' 
	'sprintf.o' "'snprintf.o'" 'vsprintf.o' 'vsnprintf.o' 
	)