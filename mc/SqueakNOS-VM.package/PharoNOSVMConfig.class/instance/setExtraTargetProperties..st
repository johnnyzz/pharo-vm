utils
setExtraTargetProperties: maker

	maker cmd: 'add_dependencies' params: maker moduleName , ' libc-objects'.

	maker setTargetProperties: 'LINK_FLAGS "-m32 -nostdlib -static -Xlinker --script=${targetPlatform}/boot/kernel.ld"'.

	maker puts: 'set_source_files_properties( ${srcVMDir}/cogit.c PROPERTIES 
		COMPILE_FLAGS "-O1 -fno-omit-frame-pointer -momit-leaf-frame-pointer -mno-rtd -mno-accumulate-outgoing-args")'.

	maker 
		cmd: 'set_source_files_properties'
		params: ' ${targetPlatform}/vm/sqUnixHeartbeat.c PROPERTIES 
		COMPILE_FLAGS "-O1 -fno-omit-frame-pointer -mno-rtd -mno-accumulate-outgoing-args"'.
	
	maker set: 'EXECUTABLE_OUTPUT_PATH' toString: '${outputDir}'.
