sources
platformSources
	"files added from platform/unix/vm dir "
	^ #(
	'mini-printf.c'
	'console.c'
	'framebuffer.c'
	'ints.c'
	'nos.c'
	'paging.c'
	'serial.c'
	'sqGlue.c'
	'sqGlue2.c'
	'sqLibc.c'
	'sqLibc2.c'
	'sqMinimal.c'
	'sqPlatformSpecific.c'
	)