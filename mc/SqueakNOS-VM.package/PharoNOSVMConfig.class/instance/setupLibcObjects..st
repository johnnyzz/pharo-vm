utils
setupLibcObjects: maker

	maker set: 'libcObjects' to: self libcObjectsFullPath.

	maker cmd: 'add_custom_command' params: 'OUTPUT ${libcObjects}
		COMMAND ${CMAKE_AR} x libc.a ', self libcObjectsString , '
		DEPENDS ', self libcName ,'
		COMMENT "Unpacking LibC objects ... "
		WORKING_DIRECTORY "${thirdpartyDir}/out/lib"
	'.
	
	maker cmd: 'add_custom_target' params: 'libc-objects DEPENDS ${libcObjects}'.