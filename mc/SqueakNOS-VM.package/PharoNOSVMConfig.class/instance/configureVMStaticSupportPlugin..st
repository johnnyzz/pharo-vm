plugin extra rules
configureVMStaticSupportPlugin: maker 
	"extra rules for VMStaticSupportPlugin" 
	| lib |
	"LIBC"
	lib := self addThirdpartyLibrary: self libcName.

	self staticLibraries do: [:tgt |
		maker addExternalLibrary: tgt.
	].

	"maker cmd: 'add_dependencies' params: 'VMStaticSupportPlugin ', self libcName."
	maker includeDirectories: lib includeDir. 
	"maker addExternalLibrary: '${thirdpartyDir}/out/lib/', lib libraryFileName."
	maker addExternalDependency: self libcName.
	
	"FREETYPE"
	"lib := self addThirdpartyLibrary: 'freetype2-static'."
	"maker includeDirectories: lib includeDir. "

	"Support"
	"maker addCrossSources: #('VMStaticSupportPlugin.c')." "not needed for NOS"

	"LIBGCC"
	lib := self addThirdpartyLibrary: 'libgcc-objects'.

