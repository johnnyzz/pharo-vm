utils
setupSmalltalkImage: maker
	
	maker cmd: 'file' params: 'MAKE_DIRECTORY "${buildDir}/image"'.
	maker cmd: 'set_property' params: 'DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "${buildDir}/image"'.
	maker cmd: 'configure_file' params: '"${originDir}/scripts/LoadNOS.st.in" "${buildDir}/image/LoadNOS.st"'.

	maker cmd: 'add_custom_command' params: 'OUTPUT "${buildDir}/image/pharo-vm" ${buildDir}/image/Pharo.image
  		COMMAND wget -O- get.pharo.org/40+vm | bash
  		WORKING_DIRECTORY "${buildDir}/image"
	'.
	
	maker cmd: 'add_custom_command' params: 'OUTPUT ${nosBaseImageFiles}
  		COMMAND ./pharo Pharo.image "LoadNOS.st"
		DEPENDS ${buildDir}/image/Pharo.image
  		WORKING_DIRECTORY "${buildDir}/image"
	'.