utils
setupDiskImage: maker

	maker cmd: 'add_custom_command' params: 'OUTPUT NOS.img
		#150 MB
		COMMAND dd if=/dev/zero of=NOS.img bs=1 count=1 seek=157286400
		COMMAND ${CMAKE_COMMAND} -E remove grub-installed.stamp
		COMMAND echo \"o\\nnew\\nprimary\\n1\\n4096\\n307000\\ntype\\nc\\na\\n1\\nprint\\nwrite\\n\" | fdisk NOS.img
		COMMAND dd conv=notrunc if=NOS.fat32 conv=notrunc of=NOS.img skip=0 seek=4096
		DEPENDS NOS.fat32
		COMMENT "Generating NOS disk image ... "
		WORKING_DIRECTORY "${buildDir}"
	'.
	
	maker cmd: 'add_custom_command' params: 'OUTPUT NOS.fat32
		COMMAND ${CMAKE_COMMAND} -E remove NOS.fat32
		COMMAND mkfs -t vfat -F 32 -n "NOS" -M 0xF8 -s 1 -R 2048 -C NOS.fat32 150000

		COMMAND ${MCOPY} -i NOS.fat32 $<TARGET_FILE:${KERNEL}> ::SqueakNOS.kernel
		COMMAND ${MCOPY} -i NOS.fat32 ${buildDir}/image/PharoNOS.image ::SqueakNOS.image
		COMMAND ${MCOPY} -i NOS.fat32 ${buildDir}/image/PharoNOS.changes ::SqueakNOS.changes
		COMMAND ${MCOPY} -i NOS.fat32 ${buildDir}/image/pharo-vm/PharoV40.sources ::PharoV40.sources
		COMMAND ${MMD} -i NOS.fat32 ::BOOT
		COMMAND ${MMD} -i NOS.fat32 ::BOOT/GRUB
		COMMAND ${MCOPY} -i NOS.fat32 ${targetPlatform}/boot/grub/stage1 ::BOOT/GRUB/STAGE1
		COMMAND ${MCOPY} -i NOS.fat32 ${targetPlatform}/boot/grub/stage2 ::BOOT/GRUB/STAGE2
		COMMAND ${MCOPY} -i NOS.fat32 ${targetPlatform}/boot/grub/fat_stage1_5 ::BOOT/GRUB/fat_stage1_5
		COMMAND ${MCOPY} -i NOS.fat32 ${targetPlatform}/boot/grub/menu.lst ::BOOT/GRUB/MENU.LST

		DEPENDS ${KERNEL} ${nosBaseImageFiles}
		COMMENT "Generating NOS filesystem ... "
		WORKING_DIRECTORY "${buildDir}"
	'.

	maker cmd: 'add_custom_command' params: 'OUTPUT grub-installed.stamp
  		COMMAND echo \"device \(hd0\) NOS.img\\nroot \(hd0,0\)\\nsetup \(hd0\)\\nquit\" | grub --batch --device-map=/dev/null
		COMMAND ${CMAKE_COMMAND} -E touch grub-installed.stamp
		COMMENT "Installing GRUB ... "
		WORKING_DIRECTORY "${buildDir}"
	'.