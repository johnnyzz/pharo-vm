image save/restore
allocateMemory: heapSize minimum: minimumMemory imageFile: fileStream headerSize: headerSize
	<export: true>

	^ super allocateMemory: heapSize minimum: minimumMemory imageFile: fileStream headerSize: headerSize