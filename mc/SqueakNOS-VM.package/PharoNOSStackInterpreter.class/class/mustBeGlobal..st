translation
mustBeGlobal: var
	"Make disownCount global so that debugging/instrumentation code can use it to check
	 if a threaded FFI call is in progress (i.e. this isn't necessary for production)"

	^(super mustBeGlobal: var)
	   or: [ #('primitiveFunctionPointer' 'memory' 
				'endOfMemory' 'memoryLimit' 'freeStart' 'youngStart'
				'specialObjectsOop' 'nilObj' 'activeContext'
				'rootTable' 'rootTableCount' 'extraRoots' 'extraRootCount'
				'messageSelector' 'method' 'methodClass' 'newMethod' 'receiver' 'receiverClass'
				'lkupClass'
				'voidVMStateForSnapshotFlushingExternalPrimitivesIf' 
			) includes: var]