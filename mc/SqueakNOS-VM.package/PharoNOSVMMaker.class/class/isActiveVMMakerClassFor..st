as yet unclassified
isActiveVMMakerClassFor: platformName

	^ (platformName = 'PharoNOS') or: [ platformName = 'NOS' ]