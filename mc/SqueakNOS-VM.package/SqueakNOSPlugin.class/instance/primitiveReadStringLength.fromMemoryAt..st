as yet unclassified
primitiveReadStringLength: length fromMemoryAt: address
	| dataOop dataPtr |

	self primitive: 'primitiveReadStringLengthFromMemoryAt'
		parameters: #(SmallInteger SmallInteger).

	self var: #dataPtr type: 'unsigned char *'.
	dataOop := interpreterProxy instantiateClass: interpreterProxy classString indexableSize: length.
	dataPtr := interpreterProxy firstIndexableField: dataOop.
	self cCode: 'memcpy(dataPtr, (void*)address, length)'.
	^ dataOop