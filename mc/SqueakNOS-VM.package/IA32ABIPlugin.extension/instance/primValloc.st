*SqueakNOS-VM
primValloc
	"Malloc arg bytes."
	"primValloc: byteSize <Integer> <^Integer>
		<primitive: 'primValloc' error: errorCode module: 'IA32ABI'>"
	| byteSize addr |
	<export: true>

	byteSize := interpreterProxy stackIntegerValue: 0.
	(interpreterProxy failed
	 or: [byteSize <= 0 "some mallocs can't deal with malloc(0) bytes"]) ifTrue:
		[^interpreterProxy primitiveFailFor: PrimErrBadArgument].
	addr := self cCode: [(self valloc: byteSize) asUnsignedInteger] inSmalltalk: [Alien Cvalloc: byteSize].
	addr = 0 ifTrue:
		[^interpreterProxy primitiveFailFor: PrimErrNoCMemory].
	interpreterProxy methodReturnValue: (self positiveMachineIntegerFor: addr)