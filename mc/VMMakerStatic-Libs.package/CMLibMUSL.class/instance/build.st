as yet unclassified
build

	gen 
		puts:
'
add_custom_command(OUTPUT "${libcbuild}"
	COMMAND mkdir -p "${libSourcesDir}/build"
)
add_custom_command(OUTPUT "${libcconfig}"
	COMMAND ../configure --prefix=''${installPrefix}'' ', self configurationFlags, '
	WORKING_DIRECTORY "${libSourcesDir}/build"
	DEPENDS "${libcbuild}" "${unpackTarget}"
)
add_custom_command(OUTPUT "${libcInstalled}"
	COMMAND make
	COMMAND make install
	WORKING_DIRECTORY "${libSourcesDir}/build"
	DEPENDS "${libcconfig}"
	COMMENT "Building ${libName}"
)
'
