as yet unclassified
allTargetsForLinking
	"Answer a target or path to library to link with, which can be used by 
	external plugins or VM module"

	^ { 
		 '${thirdpartyDir}/out/lib/', 'libfreetype.a' . "HACK! FIXME"
		 '${thirdpartyDir}/out/lib/', 'libm.a' .
		 '${thirdpartyDir}/out/lib/', 'libdl.a' . 
		 '${thirdpartyDir}/out/lib/', 'libpthread.a' .
		 'gcc_eh' .
		 self targetForLinking .
		 'gcc_eh' .
		 'gcc' }