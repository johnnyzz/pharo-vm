as yet unclassified
setVariables
	super setVariables.
	
"add include path"
	gen 
		set: #libc_includeDir toString: '${installPrefix}/include';
		set: #libraryFileName to: self libraryFileName;
		"set: #libc_location toString: '${externalModulesDir}/${libraryFileName}';"
		set: #libcbuild toString: '${libSourcesDir}/build';
		set: #libcconfig toString: '${libSourcesDir}/build/config.mak';
	 	set: #libcInstalled toString: '${installPrefix}/lib/${libraryFileName}'