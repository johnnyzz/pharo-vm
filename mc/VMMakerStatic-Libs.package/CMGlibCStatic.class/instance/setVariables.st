as yet unclassified
setVariables
	super setVariables.
	
"add include path"
	gen 
		set: #glibc_includeDir toString: '${installPrefix}/include';
		set: #libraryFileName to: self libraryFileName;
		set: #glibc_location toString: '${externalModulesDir}/${libraryFileName}';
		set: #glibcbuild toString: '${libSourcesDir}/build';
		set: #glibcconfig toString: '${libSourcesDir}/build/config.status';
	 	set: #glibcInstalled toString: '${installPrefix}/lib/${libraryFileName}'