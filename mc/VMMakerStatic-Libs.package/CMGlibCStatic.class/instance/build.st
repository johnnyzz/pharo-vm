as yet unclassified
build

	gen 
		puts:
'
add_custom_command(OUTPUT "${glibcbuild}"
	COMMAND mkdir -p "${libSourcesDir}/build"
)
add_custom_command(OUTPUT "${glibcconfig}"
	COMMAND env CFLAGS=''-nostdlib -m32 -march=i686 -O -Wa,--32'' LDFLAGS=''-march=i686 -m32 -pie -static'' ../configure --prefix=''${installPrefix}'' ', self configurationFlags, '
	WORKING_DIRECTORY "${libSourcesDir}/build"
	DEPENDS "${glibcbuild}" "${unpackTarget}"
)
add_custom_command(OUTPUT "${glibcInstalled}"
	COMMAND make
	COMMAND make install
	WORKING_DIRECTORY "${libSourcesDir}/build"
	DEPENDS "${glibcconfig}"
	COMMENT "Building ${libName}"
)
'
