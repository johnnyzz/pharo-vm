generating actions
setVariables

	gen
		set: #libName toString: self canonicalName;
		set: #libraryFileName to: self libraryFileName;
		set: #workDir toString: '${thirdpartyDir}/${libName}';
		set: #unpackedDirName toString: self unpackedDirName;
		set: #libSourcesDir toString: '${thirdpartyDir}/${libName}';
		set: #installPrefix toString: '${thirdpartyDir}/out';
		set: #libGccInstalled toString: '${installPrefix}/lib/${libraryFileName}'