generating actions
build

	gen 
		puts:
'
add_custom_command(OUTPUT "${libSourcesDir}/${libraryFileName}"
	COMMAND ${CMAKE_C_COMPILER} ', self gccObjects, ' -static -o libgcc.o -nostdlib -m32 -Xlinker -r
	DEPENDS "${unpackTarget}"
	WORKING_DIRECTORY "${libSourcesDir}"
	COMMENT "Building ${libName}"
)
'.
	self 
		copy: '${libSourcesDir}/${libraryFileName}' 
		to: '${libGccInstalled}'