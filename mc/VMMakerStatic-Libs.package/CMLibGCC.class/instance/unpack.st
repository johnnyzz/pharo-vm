generating actions
unpack

	gen set: #unpackTarget toString: '${libSourcesDir}/touch.cmake'.

	gen puts:
'add_custom_command(OUTPUT "${unpackTarget}"
		COMMAND ${CMAKE_AR} x `gcc -print-libgcc-file-name -m32` ', self gccObjects , '
		COMMAND touch "${unpackTarget}"
		COMMENT "Unpacking ${libName} ... "
		WORKING_DIRECTORY "${libSourcesDir}"
	)
'.
