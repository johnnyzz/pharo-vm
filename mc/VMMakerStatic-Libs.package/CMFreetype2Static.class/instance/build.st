generating actions
build

	gen 
		puts:
'
add_custom_command(OUTPUT "${ft2config}"
	COMMAND env CFLAGS=''-m32 -std=c99 -march=i386 -fno-stack-protector -DFT_CONFIG_OPTION_DISABLE_STREAM_SUPPORT -fno-builtin'' LDFLAGS=''-march=i386 -m32 -static -nostdlib ${thirdpartyDir}/out/lib/crt1.o -Wl,-whole-archive ${thirdpartyDir}/out/lib/libc.a -Wl,-no-whole-archive -lgcc -L${thirdpartyDir}/out/lib/'' ./configure --prefix=''${installPrefix}'' ', self configurationFlags, '
	WORKING_DIRECTORY "${libSourcesDir}"
	DEPENDS "${unpackTarget}"
)
add_custom_command(OUTPUT "${ft2libInstalled}"
	COMMAND make
	COMMAND make install
	WORKING_DIRECTORY "${libSourcesDir}"
	DEPENDS "${ft2config}"
	COMMENT "Building ${libName}"
)
'
