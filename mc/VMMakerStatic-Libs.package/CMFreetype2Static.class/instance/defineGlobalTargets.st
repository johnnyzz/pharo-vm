generating actions
defineGlobalTargets
	| var targetName |
	var := self canonicalName , '_LIB'.
	targetName := self canonicalName , '_imported'.
	vmGen set: var toString: self targetForLinking.
"
define a library as imported one
and make it depend from it's build target
"
	vmGen
		puts: 
('add_library("{1}" STATIC IMPORTED GLOBAL)
	set_target_properties("{1}" PROPERTIES IMPORTED_LOCATION "{2}")
add_dependencies("{1}" "{3}")
' format: { targetName . '${', var, '}'  . self buildTarget }
).

	vmGen cmd: 'add_dependencies' params:
		self buildTarget , ' ' , self libcName.
	vmGen cmd: 'add_dependencies' params:
		vmGen moduleName , ' ' , self buildTarget
