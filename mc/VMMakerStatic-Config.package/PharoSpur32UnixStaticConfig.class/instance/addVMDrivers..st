utils
addVMDrivers: maker

	 self 
		addDriver: 'vm-display-null' 
		sources: #( 
			'${targetPlatform}/vm-display-null/sqUnixDisplayNull' )
		generator: maker
		externalLibs: #();
		
		addDriver: 'vm-sound-null' 
		sources: #( 
			'${targetPlatform}/vm-sound-null/sqUnixSoundNull' )
		generator: maker
		externalLibs: #().
		
"		addDriver: 'vm-display-fbdev'
		sources: #(  
			'${targetPlatform}/vm-display-fbdev/sqUnixFBDev.c' )
		generator: maker
		externalLibs: #()."