utils
setExtraTargetProperties: maker

	maker puts: ('set_property(TARGET PROPERTY CMAKE_C_FLAGS "{1}")
		' format: {'-I${thirdpartyDir}/out/include' }
	).
	maker set: 'EXECUTABLE_OUTPUT_PATH' toString: '${outputDir}'.
	maker set: 'CMAKE_EXE_LINKER_FLAGS' toString: '${CMAKE_EXE_LINKER_FLAGS} -nostdlib'.
	self addVMDrivers: maker.