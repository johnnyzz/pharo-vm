settings
extraVMSettings: maker
	super extraVMSettings: maker.

	maker includeDirectories:  '${thirdpartyDir}/out/include'.
	maker linkDirectories: '${thirdpartyDir}/out/lib/'.
