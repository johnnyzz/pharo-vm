plugin extra rules
configureVMStaticSupportPlugin: maker 
	"extra rules for VMStaticSupportPlugin" 
	| lib |
	"LIBC"
	lib := self addThirdpartyLibrary: self libcName.
	
	self staticLibraries do: [:tgt |
		maker addExternalLibrary: tgt.
	].
	maker includeDirectories: lib includeDir.

	"FREETYPE"
	"lib := self addThirdpartyLibrary: 'freetype2-static'."
	"maker addExternalLibrary: lib targetForLinking."
	"maker includeDirectories: lib includeDir."

	"Support"
	maker addCrossSources: #('VMStaticSupportPlugin.c').
