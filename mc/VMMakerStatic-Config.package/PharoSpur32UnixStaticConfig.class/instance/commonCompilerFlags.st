compiler flags
commonCompilerFlags
	^ super commonCompilerFlags, { 
		'-nostdlib' . 
		'-nostdinc' . 
		'-U_FORTIFY_SOURCE' .
		'-DNOEXECINFO' .
		'-DAVOID_OPENGL_H=1' .
		'-DHEADLESS' .
	"	'-DDEBUG=2' ."
		'-I${thirdpartyDir}/out/include' }