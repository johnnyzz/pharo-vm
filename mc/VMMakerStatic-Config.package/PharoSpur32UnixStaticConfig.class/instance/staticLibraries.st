accessing
staticLibraries
	^ { 
		 '${thirdpartyDir}/out/lib/', 'crt1.o' .
		 '${thirdpartyDir}/out/lib/', 'libm.a' .
		 '${thirdpartyDir}/out/lib/', 'libdl.a' . 
		 '${thirdpartyDir}/out/lib/', 'libpthread.a' .
		 'gcc_eh' . 'c' . 'gcc_eh' . 'gcc' 
		}
	