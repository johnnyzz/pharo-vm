*VMMakerStatic-Config
buildUnixStatic32
	PharoSpur32UnixStaticConfig new
		generationType: generationType;
		addExternalPlugins: #( );
		addInternalPlugins: #( UnixOSProcessPlugin FT2Plugin "SqueakSSLPlugin" );
		addThirdpartyLibraries: #( 
			'libssh2' 
			'libgit2'
			'libsdl2');
		generateSources; 
		generate.
